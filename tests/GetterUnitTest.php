<?php
require_once 'MefworksUnitTest.php';
require_once 'helpers/properties.php';

class GetterUnitTest extends MefworksTestCase
{
	public function testBaseGetter()
	{
		$a = new A();
		$this->assertEquals('A::foo', $a->foo);
	}

	public function testOverriddenGetter()
	{
		$b = new B();
		$this->assertEquals('B::foo', $b->foo);
	}

	public function testInheritedGetter()
	{
		$b = new B();
		$this->assertEquals('A::bar', $b->bar);
	}

	public function testExtendedGetter()
	{
		$b = new B();
		$this->assertEquals('A::barB::bar', $b->barBar);
	}

	public function testIsSet()
	{
		$a = new A();
		$this->assertTrue(isset($a->foo));
	}
}

