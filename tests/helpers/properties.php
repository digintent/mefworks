<?php
require_once __DIR__.'/../../lib/Getter.php';
require_once __DIR__.'/../../lib/Setter.php';

class A
{
	use \mef\Getter;

	protected function __getFoo()
	{
		return 'A::foo';
	}

	protected function __getBar()
	{
		return 'A::bar';
	}
}

class B extends A
{
	protected function __getFoo()
	{
		return 'B::foo';
	}

	protected function __getBarBar()
	{
		return parent::__getBar() . 'B::bar';
	}
}