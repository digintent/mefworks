<?php
require_once(__DIR__.'/../lib/Cache.php');
require_once(__DIR__.'/../lib/Cache/Array.php');
require_once(__DIR__.'/../lib/AutoLoader.php');

class AutoLoaderTest extends \PHPUnit\Framework\TestCase
{
    public function testInitialize()
    {
        $cache = new mef\ArrayCache;
        $autoloader = new mef\AutoLoader($cache);
        $autoloader->addSearchPath(__DIR__.'/AutoLoaderTest');

        $this->assertTrue(in_array([$autoloader, 'load'], spl_autoload_functions()));

        return $autoloader;
    }

    /**
     * @depends testInitialize
     */
    public function testInstantiateClass($autoloader)
    {
        $obj = new NoNameSpace1;
        $this->assertTrue($obj instanceof NoNameSpace1);

        $obj = new NoNameSpace2;
        $this->assertTrue($obj instanceof NoNameSpace2);

        $obj = new Test\NameSpace1;
        $this->assertTrue($obj instanceof Test\NameSpace1);
    }

    /**
     * @depends testInitialize 
     */
    public function testUnregister($autoloader)
    {
        $autoloader->unregister();
        $this->assertFalse(in_array([$autoloader, 'load'], spl_autoload_functions()));
    }
}
?>
