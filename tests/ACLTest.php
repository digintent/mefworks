<?php
require_once 'MefworksUnitTest.php';

class ACLTest extends MefworksTestCase
{
	public function setUp(): void
	{
		parent::setUp();

		$this->acl = new mef\ACL\Manager();
	}

	public function testNull()
	{
		$this->assertFalse($this->acl->isAllowed('foo'));
	}

	public function testWideOpen()
	{
		$this->acl->grant('admin');

		$this->assertFalse($this->acl->isAllowed('fakeRole'));

		$this->assertTrue($this->acl->isAllowed('admin'));
		$this->assertTrue($this->acl->isAllowed('admin', 'foo'));
		$this->assertTrue($this->acl->isAllowed('admin', 'foo', 'bar'));
		$this->assertTrue($this->acl->isAllowed('admin', null, 'bar'));
	}

	public function testOverride()
	{
		$this->acl->grant('admin');
		$this->assertTrue($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->revoke('admin', 'superpower', 'fly');
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->grant('admin', 'superpower');
		$this->assertTrue($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->revoke('admin', 'superpower', 'fly');
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));

		$this->acl->grant('admin', 'superpower', null, null, false);
		$this->assertFalse($this->acl->isAllowed('admin', 'superpower', 'fly'));		
	}

	public function testInheritence()
	{
		// A wizard can do anything with a staff, except sell it
		$this->acl->grant('wizard', 'staff');
		$this->acl->revoke('wizard', 'staff', 'sell');

		// A warrior can do anything with a sword
		$this->acl->grant('warrior', 'sword');
		$this->acl->grant('dwarf', 'axe');

		// A merchant can sell anything
		$this->acl->grant('merchant', null, 'sell');

		// A wizard-warrior is born
		$this->acl->addRole('wizard-warrior', ['wizard', 'warrior']);

		// Cannot do anything with an axe
		$this->assertFalse($this->acl->isAllowed('wizard-warrior', 'axe'));

		// Can do something with a staff
		$this->assertTrue($this->acl->isAllowed('wizard-warrior', 'staff'));

		// Cannot sell a staff (explicitly denied)
		$this->assertFalse($this->acl->isAllowed('wizard-warrior', 'staff', 'sell'));

		// But a merchant can sell a staff
		$this->assertTrue($this->acl->isAllowed('merchant', 'staff', 'sell'));

		// And now so can the wizard-warrior
		$this->acl->addRole('wizard-warrior', 'merchant');
		$this->assertTrue($this->acl->isAllowed('wizard-warrior', 'staff', 'sell'));		
	}

	public function testAssertion()
	{
		$this->acl->grant('user', 'profile', 'view', new TrueACLAssertion());
		$this->assertTrue($this->acl->isAllowed('user', 'profile', 'view'));

		$this->acl->grant('user', 'profile', 'edit', new FalseACLAssertion());
		$this->assertFalse($this->acl->isAllowed('user', 'profile', 'edit'));
	}

	public function testGraphIterator()
	{
		$iterator = new mef\ACL\GraphIterator([
			'a' => ['b', 'c'],
			'b' => ['d']
		], 'a');

		$values = [];
		foreach ($iterator as $key => $val)
		{
			if ($key == 0)
				$iterator->queueParents();

			$values[] = $val;
		}

		$this->assertEquals($values, ['a', 'b', 'c']);
	}

	public function testRole()
	{
		$role = new mef\ACL\Role('admin');
		$this->assertEquals('admin', (string) $role);
	}

	public function testResource()
	{
		$resource = new mef\ACL\Resource('account');
		$this->assertEquals('account', (string) $resource);
	}
}

require_once __DIR__.'/../lib/ACL/AssertionInterface.php';
class TrueACLAssertion implements \mef\ACL\AssertionInterface
{
	public function assert(
		mef\ACL\Manager $acl,
		mef\ACL\RoleInterface $role = null,
		mef\ACL\ResourceInterface $resource = null,
		$privilege = null)
	{
		return true;
	}
}

class FalseACLAssertion implements \mef\ACL\AssertionInterface
{
	public function assert(
		mef\ACL\Manager $acl,
		mef\ACL\RoleInterface $role = null,
		mef\ACL\ResourceInterface $resource = null,
		$privilege = null)
	{
		return false;
	}
}