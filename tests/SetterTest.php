<?php
require_once 'MefworksUnitTest.php';
require_once __DIR__.'/../lib/Setter.php';

class SetterTest extends MefworksTestCase
{
	use mef\Setter;
	private $__value;

	public function testBaseSetter()
	{
		$this->value = 42;
		$this->assertEquals(42, $this->__value);
	}

	private function __setValue($value)
	{
		$this->__value = $value;
	}
}