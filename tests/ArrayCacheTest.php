<?php
require_once 'MefworksUnitTest.php';

class ArrayCacheTest extends MefworksTestCase
{
	public function testInitialize()
	{
		$cache = new mef\ArrayCache();
		$this->assertTrue($cache instanceof mef\ArrayCache);
		return $cache;
	}

	/**
	 * @depends testInitialize
	 */
	public function testSetAndGet(mef\ArrayCache $cache)
	{
		$cache->set('foo', 42);
		$this->assertEquals($cache->get('foo'), 42);
		return $cache;
	}

	/**
	 * @depends testSetAndGet
	 */
	public function testCount(mef\ArrayCache $cache)
	{
		$this->assertEquals(count($cache), 1);
		return $cache;
	}

	/**
	 * @depends testSetAndGet
	 */
	public function testDelete(mef\ArrayCache $cache)
	{
		$cache->delete('foo');
		$this->assertNull($cache->get('foo'));
		return $cache;
	}
}