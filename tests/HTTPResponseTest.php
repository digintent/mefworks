<?php
require_once 'MefworksUnitTest.php';

class HTTPResponseTest extends MefworksTestCase
{
	public function testDefaultStatusCode()
	{
		$response = new mef\HTTP\Response;

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertEquals('OK', $response->getStatusMessage());
	}

	public function testSetStatus()
	{
		$response = new mef\HTTP\Response;

		$response->setStatus(404);

		$this->assertEquals(404, $response->getStatusCode());
		$this->assertEquals('Not Found', $response->getStatusMessage());

		$message = 'Internal Error';

		$response->setStatus(500, $message);
		$this->assertEquals(500, $response->getStatusCode());
		$this->assertEquals($message, $response->getStatusMessage());
	}

	public function testGetStatus()
	{
		$response = new mef\HTTP\Response;

		$response->setStatus(404);

		$this->assertSame('404 Not Found', $response->getStatus());

		$response->setStatus(404, 'File Not Found');

		$this->assertEquals('404 File Not Found', $response->getStatus());
	}

	public function testStatusString()
	{
		$response = new mef\HTTP\Response;
		$response->setStatus(404);

		$this->assertSame(1, preg_match('/^(.+)\r\n/', (string) $response, $m));
		$this->assertEquals('HTTP/1.1 404 Not Found', $m[1]);

		$response->setStatus(500, ' Internal Server Error ');
		$this->assertSame(1, preg_match('/^(.+)\r\n/', (string) $response, $m));
		$this->assertEquals('HTTP/1.1 500 Internal Server Error', $m[1]);
	}

	public function testNewLinesInStatusMessage()
	{
		$response = new mef\HTTP\Response;
		$response->setStatus(200, "OK\r\nSecond Line\nThird Line\r");

		$this->assertEquals('OK Second Line Third Line', $response->getStatusMessage());
	}

	public function testRedirect()
	{
		$response = new mef\HTTP\Response;

		$response->redirect('/foo');
		$this->assertEquals('/foo', $response->headers['Location']);
		$this->assertEquals(302, $response->getStatusCode());

		$response->redirect('/foo', 301);
		$this->assertEquals(301, $response->getStatusCode());

		$response->redirect('/foo', 200, 'OK');
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertEquals('200 OK', $response->getStatus());
	}
}
