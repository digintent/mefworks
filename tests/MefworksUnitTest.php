<?php
require_once(__DIR__.'/../lib/Cache.php');
require_once(__DIR__.'/../lib/Cache/File.php');
require_once(__DIR__.'/../lib/AutoLoader.php');

abstract class MefworksTestCase extends \PHPUnit\Framework\TestCase
{
	protected $autoloader;
	protected $cache;

	protected function setUp(): void
	{		
		$this->cache = new mef\FileCache('/tmp/mefclasses');
        $this->autoloader = new mef\AutoLoader($this->cache);
        if (!count($this->cache))
        	$this->autoloader->addSearchPath(__DIR__.'/../lib');
	}

	protected function tearDown(): void
	{
		if ($this->autoloader)
		{
			$this->autoloader->unregister();
			$this->autoloader = null;
		}

		$this->cache = null;
	}
}