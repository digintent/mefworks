<?php
require_once 'MefworksUnitTest.php';

class FileCacheTest extends MefworksTestCase
{
	public function testInitialize()
	{
		$cache = new mef\FileCache('/tmp/filecachetest');
		$this->assertTrue($cache instanceof mef\FileCache);
		return $cache;
	}

	/**
	 * @depends testInitialize
	 */
	public function testSetAndGet(mef\FileCache $cache)
	{
		$cache->set('foo', 42);
		$this->assertEquals($cache->get('foo'), 42);
		return $cache;
	}

	/**
	 * @depends testSetAndGet
	 */
	public function testCount(mef\FileCache $cache)
	{
		$this->assertEquals(count($cache), 1);
		return $cache;
	}

	/**
	 * @depends testSetAndGet
	 */
	public function testDelete(mef\FileCache $cache)
	{
		$cache->delete('foo');
		$this->assertNull($cache->get('foo'));
		return $cache;
	}

	public function testFailedLock()
	{
		$cache = null;
		
		$fp_lock = fopen("/tmp/filecachetest.lock", 'w+');
		flock($fp_lock, LOCK_EX);
		try 
		{
			$cache = new mef\FileCache('/tmp/filecachetest', 0.01);
		}
		catch (Exception $e) {}

		$this->assertNull($cache);

		fclose($fp_lock);
	}
}