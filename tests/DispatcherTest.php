<?php
require_once 'MefworksUnitTest.php';
require_once __DIR__.'/../lib/MVC/Controller.php';

class DispatcherTestController extends mef\MVC\Controller
{
	public function testAction(mef\MVC\MatchedRoute $match)
	{
		$this->response = $match->params['foo'];
	}
}

class TestControllerFactory implements mef\MVC\ControllerFactoryInterface
{
	public function createController(mef\MVC\Request $request, mef\MVC\MatchedRoute $matchedRoute, mef\MVC\Dispatcher $dispatcher)
	{
		return new DispatcherTestController($request, $dispatcher);
	}
}

class DispatcherTest extends MefworksTestCase
{
	public function testConstructor()
	{
		$dispatcher = new mef\MVC\Dispatcher();
		$this->assertTrue($dispatcher instanceof mef\MVC\Dispatcher);
		return $dispatcher;
	}

	/**
	 * @depends testConstructor
	 */
	public function testNoRoutes($dispatcher)
	{
		$resp = $dispatcher->process(new mef\MVC\Request('/'));

		$this->assertNull($resp);
	}

	/**
	 * @depends testConstructor
	 */
	public function testAddRouter($dispatcher)
	{
		$router = new mef\MVC\RouterRouter();
		$nullRouter = new mef\MVC\RouteRouter();
		$nullRouter->routes[] = new mef\MVC\NullRoute();
		$router->routers[] = $nullRouter;
		
		$sinkRouter = new mef\MVC\RouteRouter();
		$sinkRouter->routes[] = new mef\MVC\DefaultRoute('DispatcherTest', 'test', ['foo' => 'bar']);
		$router->routers[] = $sinkRouter;

		$dispatcher->router = $router;

		$this->assertEquals($router, $dispatcher->router);
		
		return $dispatcher;
	}

	/**
	 * @depends testAddRouter
	 */
	public function testProcess($dispatcher)
	{
		$resp = $dispatcher->process(new mef\MVC\Request('/'));

		$this->assertEquals('bar', $resp);
	}

	/**
	 * @depends testAddRouter
	 */
	public function testControllerFactory($dispatcher)
	{
		$factory = new TestControllerFactory;
		$dispatcher->setControllerFactory($factory);

		$this->assertEquals($factory, $dispatcher->getControllerFactory());

		$resp = $dispatcher->process(new mef\MVC\Request('/'));

		$this->assertEquals('bar', $resp);
	}

	/**
	 * @depends testAddRouter
	 */
	public function testRemoveRouter($dispatcher)
	{
		$router = new mef\MVC\NullRouter();
		$dispatcher->router = $router;

		$this->assertEquals($dispatcher->router, $router);

		return $dispatcher;
	}

	/**
	 * @depends testRemoveRouter
	 */
	public function testProcess404($dispatcher)
	{
		$sinkRouter = new mef\MVC\RouteRouter();
		$sinkRouter->routes[] = new mef\MVC\DefaultRoute('DispatcherTest', 'missing');
		$dispatcher->router = $sinkRouter;

		$resp = $dispatcher->process(new mef\MVC\Request('/'));

		$this->assertNull($resp);
	}
}

