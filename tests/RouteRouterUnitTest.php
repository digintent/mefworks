<?php
require_once 'MefworksUnitTest.php';

class RouteRouterUnitTest extends MefworksTestCase
{
	public function testInitialize()
	{
		$router = new mef\MVC\RouteRouter();
		$this->assertTrue($router instanceof mef\MVC\Router);

		return $router;
	}

	/**
	 * @depends testInitialize
	 */
	public function testNoRoute(mef\MVC\RouteRouter $router)
	{
		$req = new mef\MVC\Request('/');
		$match = $router->firstMatch($req);
		$this->assertNull($match);

		return $router;
	}

	/**
	 * @depends testInitialize
	 */
	public function testAddRoute(mef\MVC\RouteRouter $router)
	{
		$route = new mef\MVC\DefaultRoute('Controller', 'Action');
		$router->routes[] = $route;

		$this->assertEquals(count($router->routes), 1);
		$this->assertEquals($router->routes[0], $route);

		return $router;
	}

	/**
	 * @depends testAddRoute
	 */
	public function testFindMatch(mef\MVC\RouteRouter $router)
	{
		$req = new mef\MVC\Request('/foo/bar');
		$match = $router->firstMatch($req);
		$this->assertNotNull($match);
		
		return $router;
	}
}