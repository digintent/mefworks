<?php namespace mef\BarCode;

use Exception;
use InvalidArgumentException;

/**
 * A QR Encoder.
 *
 * http://en.wikipedia.org/wiki/QR_code
 *
 * Uses Kentaro Fukuchi's qrencode program from http://fukuchi.org/works/qrencode/index.html.en
 * to encode the images.
 *
 */
class QREncoder extends EncoderAbstract
{
	protected $__qrencodePath = 'qrencode';

	protected function __getQrEncodePath()
	{
		return $this->__qrencodePath;
	}

	protected function __setQrEncodePath($path)
	{
		$this->__qrencodePath = (string) $path;
	}

	protected $__errorCorrection = 'M';

	protected function __getErrorCorrection()
	{
		return $this->__errorCorrection;
	}

	protected function __setErrorCorrection($errorCorrection)
	{
		if (!in_array($errorCorrection, ['L','M','Q','H']))
			throw new InvalidArgumentException('errorCorrection');

		$this->__errorCorrection = $errorCorrection;
	}

	protected $__scale = 3;

	protected function __getScale()
	{
		return $this->__scale;
	}

	protected function __setScale($scale)
	{
		$scale = (int) $scale;
		if ($scale < 1)
			throw new InvalidArgumentException('scale');

		$this->__scale = $scale;
	}

	public function encodeToStream($stream, $data)
	{
		$desc = [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']];
		$proc = proc_open($this->__qrencodePath.' -o - -l '.$this->__errorCorrection.' -s '.$this->__scale, $desc, $pipes);
		if (!$proc)
			throw new Exception("Unable to open ".$this->__qrEncodePath);

		fwrite($pipes[0], $data);
		fclose($pipes[0]);

		stream_copy_to_stream($pipes[1], $stream);
		fclose($pipes[1]);

		$stderr = '';
		while (!feof($pipes[2]))
			$stderr .= fread($pipes[2], 4096);
		fclose($pipes[2]);

		$status = proc_close($proc);
		if ($status)
			throw new Exception($stderr);		
	}
}