<?php namespace mef\BarCode;

use Exception;

/**
 * Abstract class representing an object capable of encoding a bar code.
 *
 * Known concrete implementations:
 *
 *   mef\BarCode\QREncoder
 */
abstract class EncoderAbstract
{
	use \mef\Getter;
	use \mef\Setter;

	/**
	 * Encodes the given data, returning as a binary string.
	 *
	 * The returned data is in the requested image format, e.g. PNG.
	 * The functions encodeToFile or encodeToStream are better suited
	 * if you want to encode directly to a file.
	 *
	 * @param string $data   The data to encode
	 *
	 * @return string        The image data
	 */
	public function encode($data)
	{
		ob_start();
		$this->encodeToFile('php://output', $data);
		return ob_get_clean();
	}

	/**
	 * Encodes the given data to a file.
	 *
	 * The filename will be overwritten if it already exists.
	 *
	 * @param string $filename   The filename to save the image
	 * @param string $data       The data to encode
	 */
	public function encodeToFile($filename, $data)
	{
		$fp = fopen($filename, 'w');
		if (!$fp)
			throw new Exception("Unable to open $filename for writing");

		$this->encodeToStream($fp, $data);
		fclose($fp);
	}

	/**
	 * Encodes to the given data to a stream.
	 * 
	 * @param stream $stream   A writable PHP stream
	 * @param string $data     The data to encode
	 */
	abstract public function encodeToStream($stream, $data);
}
