<?php namespace mef\MVC;

use Exception;

class CLIRequest extends Request
{
	static public function getCurrent()
	{
		$options = getopt('', ['uri:']);

		if (!isset($options['uri']))
			throw new Exception('Missing uri parameter');

		return new static($options['uri']);
	}
}