<?php namespace mef\MVC;

use \mef\Net\URI;

class Request
{
	use \mef\Getter;
	use \mef\Setter;
	
	protected $__uri;
	
	public function __construct($uri)
	{
		$this->uri = $uri;
	}
	
	protected function __setURI($uri)
	{
		if (!($uri instanceof URI))
			$uri = new URI((string) $uri);
			
		$this->__uri = $uri;
	}
	
	protected function __getURI()
	{
		return $this->__uri;
	}
}