<?php namespace mef\MVC;

use stdClass;

class WebController extends Controller
{
	protected $db;
	protected $container = 'container.php';
	protected $view_context;
	
	public function __construct()
	{
		$this->view_context = new stdClass;
	}
	
	public function initializeRequest(\mef\MVC\Request $req)
	{
		$resp = new \mef\HTTP\Response();
		$resp->headers['Content-type'] = 'text/html';
		
		if (is_string($this->container))
		{
			$this->container = $this->view($this->container);
		}
		
		return $resp;
	}
	
	public function finalizeResponse(\mef\MVC\Response $resp)
	{
		foreach ($resp->headers->asArray() as $name => $value)
			header("$name: $value");

		$this->container->set('contents', $resp->body);
		$this->container->render();
	}
	
	protected function model($model_name)
	{
		static $factories = [];

		if (!array_key_exists($model_name, $factories))
			$factories[$model_name] = new \ModelFactory($model_name, $this->db); 		
		
		return $factories[$model_name];
	}
	
	protected function view($filename)
	{
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		$filename = __DIR__.'/../../../'.$filename;
		
		if ($ext == 'php')
			return new PHPView($filename, $this->view_context);
		else if ($ext == 'html')
			return new HTMLView($filename);
		else
			return new TextView($filename);
	}
}