<?php namespace mef\MVC;

abstract class Route
{
	abstract public function matches(Request $req);
}