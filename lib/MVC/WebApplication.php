<?php namespace mef\MVC;

class WebApplication extends Application
{
	public function run(Request $req = null)
	{
		if ($req == null)
			$req = mef\HTTP\Request::getCurrent();

		parent::run($req);
	}
}