<?php namespace mef\MVC;

class MatchedRoute
{
	use \mef\Getter;

	public function __construct($controller, $action, array $params = [])
	{
		$this->__controller = (string) $controller;
		$this->__action = (string) $action;
		$this->__params = $params;
	}

	/**
	 * @var string $controller   The name of the controller
	 */
	protected $__controller;

	final protected function __getController()
	{
		return $this->__controller;
	}

	/**
	 * @var string $action       The name of the action
	 */
	protected $__action;

	final protected function __getAction()
	{
		return $this->__action;
	}

	/**
	 * @var array $params        An array of arbitrary parameters
	 */
	protected $__params;

	final protected function __getParams()
	{
		return $this->__params;
	}
}