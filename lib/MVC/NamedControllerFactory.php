<?php namespace mef\MVC;

class NamedControllerFactory implements ControllerFactoryInterface
{
	private $formatter;

	public function __construct(callable $formatter)
	{
		$this->formatter = $formatter;
	}

	public function createController(Request $request, MatchedRoute $matchedRoute, Dispatcher $dispatcher)
	{
		$className = call_user_func($this->formatter, $request, $matchedRoute);

		try 
		{
			$controllerReflector = new \ReflectionClass($className);
		}
		catch (\ReflectionException $e)
		{
			return null;
		}

		if (!$controllerReflector->isSubclassOf('mef\\MVC\\Controller'))
		{
			return null;
		}

		return $controllerReflector->newInstance($request, $dispatcher);
	}
}