<?php namespace mef\MVC;

/**
 * A Router routes a [mef\MVC\Request] to an action in a [mef\MVC\Controller].
 *
 * It is not responsible for determining whether or not either the controller or
 * action is valid. Normally a [mef\MVC\Dispatcher] would iterate over its own
 * Router, try to instantiate the controller, and invoke the action.
 *
 * @see mef\MVC\RouteRouter - The most common router type.
 */
abstract class Router
{
	/**
	 * Returns an iterator of matches.
	 * 
	 * The concrete classes must override this method to provide the iterator.
	 *
	 * @param Request $req  the request to match
	 * 
	 * @return Iterator     a list of matches
	 */
	abstract public function match(Request $req);

	/**
	 * A convenience function that returns the first match.
	 *
	 * @param mef\MVC\Request $req  The request for which to find a match.
	 *
	 * @return stdClass|null  If a match was found, return it. 
	 *                        Else return null.
	 */
	final public function firstMatch(Request $req)
	{
		$match = null;
		foreach ($this->match($req) as $match)
			break;
		
		return $match;
	}
}