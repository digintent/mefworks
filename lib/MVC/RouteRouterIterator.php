<?php namespace mef\MVC;

use ArrayIterator;
use FilterIterator;

/**
 * The RouteRouterIterator iterates over all the routes,
 * testing each one to see if it matches the request.
 *
 * The values are the match object, as opposed to the route itself.
 */
class RouteRouterIterator extends FilterIterator
{
	/**
	 * @var Request $req   The request to test against. set by the
	 *                     constructor and is immutable.
	 */
	private $req;

	/**
	 * @var stdClass currentMatch   Set by accept() on each iteration
	 *                              and returned by current().
	 */
	private $currentMatch;

	/**
	 * Construct the iterator given the Router and Request.
	 * Normally called by Router::match().
	 *
	 * @param mef\MVC\RouteRouter  $router  The list of routes
	 * @param mef\MVC\Request $req     The request to test against
	 *
	 * @see mef\MVC\Router::match()
	 */
	public function __construct(RouteRouter $router, Request $req)
	{
		parent::__construct(new ArrayIterator($router->routes));
		$this->req = $req;
	}

	/**
	 * Test the inner iterator, filtering out those that don't match.
	 *
	 * @return boolean   true if the route matches
	 */
	public function accept(): bool
	{
		$route = $this->getInnerIterator()->current();
		$this->currentMatch = $route->matches($this->req);
		return $this->currentMatch !== false;
	}

	/**
	 * Overrides the default behavior (of returning the Route) to
	 * return the match object.
	 *
	 * @return stdClass  The match associated with the current route
	 */
	public function current(): mixed
	{
		return $this->currentMatch;
	}
}