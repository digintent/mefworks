<?php namespace mef\MVC;

use ArrayObject;
use ReflectionClass;
use ReflectionException;

class Dispatcher
{
	use \mef\Getter, \mef\Setter;

	/**
	 * Dispatcher constructor
	 *
	 * @param mef\MVC\Router $router   The router to use when processing requests.
	 */
	public function __construct(Router $router = null)
	{
		$this->__router = $router ?: new NullRouter();
	}

	/**
	 * @var $router mef\MVC\Router  The router used by the dispatcher. If multiple routers
	 *                              are needed, then a RouterRouter should be used.
	 */
	protected $__router;

	final protected function __getRouter()
	{
		return $this->__router;
	}

	final protected function __setRouter(Router $router)
	{
		$this->__router = $router;
	}

	/**
	 * @var callable $controllerNameFormatter
	 */
	protected $__controllerNameFormatter;

	final protected function __getControllerNameFormatter()
	{
		return $this->__controllerNameFormatter;
	}

	final protected function __setControllerNameFormatter(callable $controllerNameFormatter)
	{
		$this->__controllerNameFormatter = $controllerNameFormatter;
	}

	/**
	 * @var mef\MVC\ControllerFactoryInterface $controllerFactory
	 */
	private $controllerFactory;

	/**
	 * Return the current controller factory.
	 *
	 * @return mef\MVC\ControllerFactoryInterface
	 */
	final public function getControllerFactory()
	{
		return $this->controllerFactory;
	}

	/**
	 * Set the controller factory.
	 *
	 * @param ControllerFactoryInterface $controllerFactory
	 */
	final public function setControllerFactory(ControllerFactoryInterface $controllerFactory)
	{
		$this->controllerFactory = $controllerFactory;
	}

	/**
	 * Process the request by asking the router for a match.
	 *
	 * Routes are processed in the same order as they were added to the router.
	 * If a matched route does not handle the request, then the next matched route
	 * is tried.
	 *
	 * @param mef\MVC\Request   The request to process
	 *
	 * @return mixed
	 */
	public function process(Request $req)
	{
		foreach ($this->__router->match($req) as $params)
		{
			$resp = $this->handleRequest($req, $params);
			if ($resp !== false) return $resp;
		}

		return null;
	}

	/**
	 * Responsible for creating the controller and calling the action function.
	 * The controller
	 *
	 * @param mef\MVC\Request       The request being processed
	 * @param mef\MVC\MatchedRoute  The matched route object from the router
	 *
	 * @return mixed    false if the request wasn't actually handled
	 *                  else, whatever the controller responded with.
	 */
	protected function handleRequest(Request $req, MatchedRoute $matchedRoute)
	{
		if ($this->controllerFactory !== null)
		{
			$controllerFactory = $this->controllerFactory;
		}
		else
		{
			$formatter = $this->controllerNameFormatter;

			if (!$formatter)
			{
				$formatter = function (Request $request, MatchedRoute $matchedRoute)
				{
					return $matchedRoute->controller.'Controller';
				};
			}

			$controllerFactory = new NamedControllerFactory($formatter);
		}

		$controller = $controllerFactory->createController(
			$req,
			$matchedRoute,
			$this
		);

		if ($controller === null)
		{
			return false;
		}

		return $controller->respond($matchedRoute);
	}
}