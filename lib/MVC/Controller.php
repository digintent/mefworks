<?php namespace mef\MVC;

use Exception;
use ReflectionMethod;
use ReflectionException;

class Controller
{
	protected $request, $response;
	protected $dispatcher;

	final public function __construct(Request $request, Dispatcher $dispatcher = null)
	{
		$this->request = $request;

		$this->dispatcher = $dispatcher;
		$this->initialize();
	}

	protected function initialize()
	{
		if (!$this->response)
			$this->response = new Response();
	}

	public function respond(MatchedRoute $params)
	{
		$action = $params->action;

		try
		{
			$reflector = new ReflectionMethod($this, str_replace('-', '', $action).'Action');
		}
		catch (ReflectionException $e)
		{
			return false;
		}

		$this->before($params);
		$reflector->invoke($this, $params);
		$this->after();

		return $this->response;
	}

	protected function before(MatchedRoute $params)
	{
	}

	protected function after()
	{
	}

	protected function call($req)
	{
		return $this->dispatcher->process(new Request($req));
	}
}