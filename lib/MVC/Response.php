<?php namespace mef\MVC;

class Response
{
	use \mef\Getter, \mef\Setter;
	
	protected $body;
	
	protected function __getBody()
	{
		return $this->body;
	}
	
	protected function __setBody($body)
	{
		$this->body = $body;
	}
}