<?php namespace mef\MVC;

use ArrayObject;

class RouteRouter extends Router
{
	use \mef\Getter;
	
	/**
	 * Create a new router.
	 *
	 * @param array $routes   An array of routes
	 */
	public function __construct(array $routes = [])
	{
		$this->__routes = new ArrayObject($routes);
	}
	
	/**
	 * @var array $routes   The list of route associated with Router
	 */
	protected $__routes;	
	final protected function __getRoutes()
	{
		return $this->__routes;
	}
	
	/**
	 * Return an iterator that iterates over all matches
	 *
	 * @param mef\MVC\Request $req  The request for which to find matches.
	 * 
	 * @return mef\MVC\RouterRequestIterator
	 */
	public function match(Request $req)
	{
		return new RouteRouterIterator($this, $req);
	}
}