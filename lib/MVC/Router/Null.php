<?php namespace mef\MVC;

/**
 * The NullRouter always returns no matches.
 *
 * This is the default router if a dispatcher is created without one.
 */
class NullRouter extends Router
{
	/**
	 * Does nothing.
	 *
	 * @param Request $req  The ignored request.
	 *
	 * @return array []     Always returns an empty array.
	 */
	public function match(Request $req)
	{
		return [];
	}
}