<?php namespace mef\MVC;

use AppendIterator;
use ArrayObject;

/**
 * A RouterRouter is a collection of routers that can be iterated over
 * sequentially. This is useful if you want to use more than one router
 * with a single dispatcher.
 *
 * Example:
 * <code>
 * $router1 = new RouteRouter();
 * $router1->domain = 'admin.example.com';
 * $router1->routes[] = new DefaultRoute('Default', 'index');
 *
 * $router2 = new RouteRouter();
 * $router2->domain = 'www.example.com';
 * $router2->routes[] = new DefaultRoute('Default', 'index');
 *
 * $router = new RouterRouter([$router1, $router2]);
 *
 * foreach ($router->match($req) as $match)
 *   echo $match->controller, ':', $match->action, "\n";
 * </code>
 */
class RouterRouter extends Router
{
	use \mef\Getter;
	
	/**
	 * Create a new router.
	 *
	 * @param array $routers   An array of routers
	 */
	public function __construct(array $routers = [])
	{
		$this->__routers = new ArrayObject($routers);
	}
	
	/**
	 * @var array $routes   The list of route associated with Router
	 */
	protected $__routers;
	final protected function __getRouters()
	{
		return $this->__routers;
	}
	
	/**
	 * Return an iterator that iterates over all matches
	 *
	 * Uses an AppendIterator to join all the routers together.
	 *
	 * @param mef\MVC\Request $req  The request for which to find matches.
	 * 
	 * @return AppendIterator
	 */
	public function match(Request $req)
	{
		$itr = new AppendIterator();
		foreach ($this->__routers as $router)
			$itr->append($router->match($req));

		return $itr;
	}
}