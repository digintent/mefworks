<?php namespace mef\MVC;

/**
 * Always returns a match, regardless of the request.
 */
class DefaultRoute extends Route
{
	protected $match;
	
	/**
	 * Builds the default route, using the passed controller and action.
	 * If any $args are supplied, they are added to the list.
	 *
	 * @param $controller string    The target controller
	 * @param $action     string    The target action
	 * @param $args       array     An array of key=>value pairs of data
	 */
	public function __construct($controller, $action, array $args = [])
	{
		$this->match = new MatchedRoute($controller, $action, $args);
	}

	/**
	 * Always returns the match as created in the constructor.
	 *
	 * @param $req Request  ignored
	 * 
	 * @return stdClass
	 */	
	public function matches(Request $req)
	{
		return $this->match;
	}
}