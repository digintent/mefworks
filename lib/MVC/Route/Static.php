<?php namespace mef\MVC;

/**
 * The StaticRoute matches a static, pre-set route against the URL's path.
 * 
 */
class StaticRoute extends Route
{
	protected $path;
	protected $params;

	public function __construct($path, array $params = [])
	{
		$this->path = $path;
		$this->params = $params;
	}

	/**
	 * Matches nothing.
	 *
	 * @param Request $req  ignored
	 *
	 * @return false
	 */
	public function matches(Request $req)
	{
		if ((string) $req->url->path == $this->path)
		{
			$params = $this->params;
			return new MatchedRoute($params['controller'], $params['action'], $params);
		}

		return false;
	}
}