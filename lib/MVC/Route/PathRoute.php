<?php namespace mef\MVC;

use mef\HTTP\Request as HTTPRequest;

class PathRoute extends Route
{
	protected $parts;
	protected $params;
	protected $strict = true;

	public function __construct($path, array $params = [])
	{
		$this->parts = preg_split('#/#', $path, -1, PREG_SPLIT_NO_EMPTY);
		if (end($this->parts) == '...')
		{
			array_pop($this->parts);
			$this->strict = false;
		}
		$this->params = $params;
	}

	public function matches(Request $req)
	{
		if ($req instanceof HTTPRequest)
		{
			if (!$req->url->path)
				return false;

			$params = $this->params;
			$path = explode('/', $req->url->path);
			$i = 0;

			foreach ($this->parts as $part)
			{
				if ($part[0] == ':')
				{
					$part = substr($part, 1);
					$capture = true;
				}
				else
				{
					$capture = false;
				}

				if (substr($part, -1) == '?')
				{
					$part = substr($part, 0, -1);
					$optional = true;
				}
				else
				{
					$optional = false;
				}

				if (count($path) <= $i)
				{
					if (!$optional)
						return false;
					else
						continue;
				}

				if ($capture)
				{
					if ($path[$i] != '')
						$params[$part] = $path[$i];
				}
				else if ($path[$i] != $part)
				{
					return false;
				}

				++$i;
			}

			if ($this->strict && $i != count($path))
				return false;

			return new MatchedRoute(
				isset($params['controller']) ? $params['controller'] : '',
				isset($params['action']) ? $params['action'] : '',
				$params
			);
		}


		return false;
	}
}
