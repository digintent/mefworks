<?php namespace mef\MVC;

class RegExpRoute extends Route
{
	protected $regexp;
	protected $data;
	
	public function __construct($regexp, array $data = [])
	{
		$this->regexp = preg_replace('/\(([A-Za-z0-9_]+):/', '(?<\1>', $regexp);
		$this->data = $data;
	}
	
	public function matches(Request $req)
	{
		if (!preg_match($this->regexp, $req->url, $m))
			return false;
			
		$match = [];
		
		foreach ($this->data as $key => $val)
		{
			$match[$key] = preg_replace_callback('/\$(\d+)/', function($i) use ($m)
			{
				return isset($m[$i[1]]) ? $m[$i[1]] : '';
			}, $val);
		}
		
		foreach ($m as $key => $val)
		{
			if (!is_numeric($key))
				$match[$key] = $val;
		}
	
		return new MatchedRoute($match['controller'], $match['action'], $match);
	}
}