<?php namespace mef\MVC;

/**
 * The NullRoute matches nothing. Serves no purpose outside of testing.
 */
class NullRoute extends Route
{
	/**
	 * Matches nothing.
	 *
	 * @param Request $req  ignored
	 *
	 * @return false
	 */
	public function matches(Request $req)
	{
		return false;
	}
}