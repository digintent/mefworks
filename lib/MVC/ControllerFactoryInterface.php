<?php namespace mef\MVC;

interface ControllerFactoryInterface
{
	public function createController(Request $request, MatchedRoute $matchedRoute, Dispatcher $dispatcher);
}