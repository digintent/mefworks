<?php namespace mef\MVC;

class Application
{
	use \mef\Getter;

	public function __construct(Dispatcher $dispatcher)
	{
		$this->__dispatcher = $dispatcher;
	}

	protected $__dispatcher;

	final protected function __getDispatcher()
	{
		return $this->__dispatcher;
	}

	public function run(Request $req)
	{
		try
		{
			$this->dispatcher->process($req);
		}
		catch (FileNotFound $e)
		{
			$fileNotFoundRequest = new Request('error://404');
			$this->dispatcher->process($fileNotFoundRequest);
		}
	}

}