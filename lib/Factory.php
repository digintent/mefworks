<?php namespace mef;

use \ReflectionClass;

Trait Factory
{
    public function factory($subclass)
    {
        static $reflectors;
        
        if (!$reflectors)
            $reflectors = array();
            
        if (!array_key_exists($subclass, $reflectors))
        {
            $reflectors[$subclass] = new ReflectionClass(__CLASS__.'_'.$subclass);
        }
		  
		  $args = array_slice(func_get_args(), 1);
       
        return count($args) ? $reflectors[$subclass]->newInstanceArgs($args) :
            $reflectors[$subclass]->newInstance();
    }
	 
	 public function factoryArgs($subclass, array $args = array())
	 {
        static $reflectors;
        
        if (!$reflectors)
            $reflectors = array();
            
        if (!array_key_exists($subclass, $reflectors))
        {
            $reflectors[$subclass] = new ReflectionClass(__CLASS__.'_'.$subclass);
        }
        
        return $reflectors[$subclass]->newInstanceArgs($args);
	 }
}