<?php namespace mef;

# Converts any object into something with chainable methods.

class MethodChainer
{
    private $obj;
    private $rv;
    private $success = true;
    
    use Getter, Setter;
    
    public function __construct($obj)
    {
        $this->obj = $obj;
    }
    
    private function __getChainee()
    {
        return $this->obj;
    }
    
    private function __getReturnValue()
    {
        return $this->rv;
    }
    
    private function __getSuccess()
    {
        return $this->success;
    }
    
    private function __setSuccess($value)
    {
        $this->success = $value ? true : false;
    }
    
    public function __call($func, $args)
    {
        $this->rv = call_user_func_array([$this->obj, $func], $args);
        $this->success = $this->success && $this->rv == true;
        
        return $this;
    }
    
}