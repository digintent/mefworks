<?php namespace mef;

trait Chainable
{
    public function chain()
    {
        return new MethodChainer($this);
    }
}