<?php namespace mef;

class AutoLoader
{
	protected $cache;
	
	public function __construct(Cache $cache, $register = true)
	{
		$this->cache = $cache;
		
		if ($register)
			$this->register();
	}
	
	public function addSearchPath($path)
	{
		$dir = new \RecursiveDirectoryIterator($path);
		$iter = new \RecursiveIteratorIterator($dir);
		$regex = new \RegexIterator($iter, '/^.+\.php$/');
		
		foreach($regex as $filename)
		{
			$code = file_get_contents($filename);
			$ns = preg_match('/\s+namespace\s+(.+);/', $code, $m) ? $m[1] : '';
			preg_match_all('/\s+(?:trait|class|interface)\s+([A-Za-z_][A-Za-z0-9_]*)/i', $code, $m);
			foreach ($m[1] as $class)
			{
				if ($ns)
					$this->cache->set(strtolower($ns.'\\'.$class), (string) $filename);
				else
					$this->cache->set(strtolower($class), (string) $filename);
			}
		}
	}
	
	public function load($class_name)
	{
		$filename = $this->cache->get(strtolower($class_name));
		if ($filename) include $filename;		
	}
	
	public function register()
	{
		spl_autoload_register([$this, 'load']);
	}
	
	public function unregister()
	{
		spl_autoload_unregister([$this, 'load']);
	}
}