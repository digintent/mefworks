<?php namespace mef;

use MemCached;

class MemoryCache implements Cache
{
	protected $mc;
	
	public function __construct(Memcached $mc)
	{
		$this->mc = $mc;
	}
	
	public function set($key, $value)
	{
		$this->mc->set($key, $value);
	}
	
	public function get($key)
	{
		return $this->mc->get($key);
	}
	
	public function delete($key)
	{
		$this->mc->delete($key);
	}
}