<?php namespace mef;

use Countable;

class ArrayCache implements Cache, Countable
{
	private $data;

	public function __construct(array $data = [])
	{
		$this->data = $data;
	}

	public function set($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function get($key)
	{
		return array_key_exists($key, $this->data) ? $this->data[$key] : null;
	}

	public function delete($key)
	{
		unset($this->data[$key]);
	}

	public function count(): int
	{
		return count($this->data);
	}

	public function asArray()
	{
		return $this->data;
	}
}