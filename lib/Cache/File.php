<?php namespace mef;

use Countable;

class FileCache implements Cache, Countable
{
	private $data = array();
	private $filename;
	private $maxLockTime;
	private $modified = false;

	public function __construct($filename, $maxLockTime = 5)
	{
		$this->filename = $filename;
		$this->maxLockTime = $maxLockTime;

		$fp_lock = fopen("$filename.lock", 'w+');
		if (!$fp_lock)
			throw new \Exception("Unable to open $filename.lock");

		$this->lock($fp_lock, LOCK_SH);

		if (file_exists($filename))
		{
			$this->data = json_decode(file_get_contents($filename), true);
		}

		flock($fp_lock, LOCK_UN);
		fclose($fp_lock);
	}

	public function __destruct()
	{
		if ($this->modified)
		{
			$filename = $this->filename;

			$fp_lock = fopen("$filename.lock", 'w+');
			if (!$fp_lock)
				throw new \Exception("Unable to open $filename.lock");

			$this->lock($fp_lock, LOCK_EX);

			file_put_contents($filename, json_encode($this->data));

			flock($fp_lock, LOCK_UN);
			fclose($fp_lock);
		}
	}

	public function set($key, $value)
	{
		$this->modified = true;
		$this->data[$key] = $value;
	}

	public function get($key)
	{
		return array_key_exists($key, $this->data) ? $this->data[$key] : null;
	}

	public function delete($key)
	{
		$this->modified = true;
		unset($this->data[$key]);
	}

	public function count(): int
	{
		return count($this->data);
	}

	/**
	 * Tries to acquire a lock within the limits of maxLockTime
	 *
	 * @param $fp_lock Resource    a handle to the lock file
	 * @param $mode    integer     LOCK_SH (read) or LOCK_EX (write)
	 *
	 * @throws Exception           if lock could not be obtained
	 */
	private function lock($fp_lock, $mode)
	{
		$time = microtime(true);
		while (!flock($fp_lock, $mode | LOCK_NB))
		{
			usleep(100);
			if (microtime(true) - $time > $this->maxLockTime)
			{
				fclose($fp_lock);
				throw new \Exception("Unable to acquire read lock on $this->filename.lock");
			}
		}
	}
}