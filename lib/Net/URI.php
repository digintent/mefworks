<?php namespace mef\Net;

class URI
{	
	protected $uri;

	public function __construct($uri)
	{
		$this->uri = $uri;
	}

	static public function cast($obj)
	{
		return ($obj instanceof static) ? $obj : new static((string) $obj);
	}
	
	public function __toString()
	{
		return $this->uri;
	}
}