<?php namespace mef\Net;

use \Exception;
use \InvalidArgumentException;

class URL extends URI
{
	use \mef\Getter { __get as getter; }
	use \mef\Setter;
	private $data = [];
	
	public function __construct($url = null)
	{
		$props = ['url','scheme','username','password','domain','port','path','query','fragment'];
		if (!$url)
		{
			$this->data = array_fill_keys($props, null);
		}
		else
		{
			preg_match('!^
				(?:([a-z0-9-]+):/{0,2})?  # scheme + optional //
				(?:                     # start username/password 
					([^@:]+)             #   username
					(?:                  #   start password
						:([^@]+)          #      password
					)?                   #   end password
				@)?                     # end username/password
				(?:
					(
						[a-z0-9](?:[a-z0-9-]*[a-z0-9])?
						(?:\.[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)*
					)                       # domain
					(?::(\d+))?             # port
					(?:/|$)
				)?
				([^?#]+)?              # path
				(?:\?([^#]+))?          # query
				(?:\#(.+))?             # fragment
			!xi', $url, $m);
			
			if (!$m)
				throw new InvalidArgumentException('Invalid URL');
			
			$data = array_combine($props, array_pad($m, 9, null));
			
			if (!$data['query'])
				$data['params'] = [];
			else 
				parse_str($data['query'], $data['params']);
				
			$this->data = $data;
		}
	}
	
	public function __get($name)
	{
		if (!array_key_exists($name, $this->data))
			return $this->getter($name);
		
		return $this->data[$name];		
	}
	
	protected function __getComplete()
	{
		return $this->scheme != '' && $this->domain != '' && $this->path != '';
	}
	
	protected function __getRelative()
	{
		return $this->domain == '' && $this->path != '';
	}
	
	protected function __setScheme($scheme)
	{
		$scheme = trim($scheme);
		if (!preg_match('/^[a-z0-9-]*$/', $scheme))
			throw new Exception("Invalid scheme format");
		
		$this->data['scheme'] = $scheme;
	}
	
	protected function __setUsername($username)
	{
		$this->data['username'] = trim($username);
	}
	
	protected function __setPassword($password)
	{
		$this->data['password'] = trim($password);
	}
	
	protected function __setDomain($domain)
	{
		$this->data['domain'] = trim($domain);
	}
	
	protected function __setPort($port)
	{
		$port = trim($port);
		if (!preg_match('/^\d*$/', $port))
			throw new Exception("Invalid port format");
		$this->data['port'] = $port;
	}
	
	protected function __setPath($path)
	{
		$this->data['path'] = trim($path);
	}
	
	protected function __setQuery($query)
	{
		$this->data['query'] = trim($query);
	}
	
	protected function __setFragment($fragment)
	{
		$this->data['fragment'] = trim($fragment);
	}
	
	public function relative($path)
	{
		$url = $this->scheme.'://';
		if ($this->username)
		{
			$url .= $this->username;
			if ($this->password)
				$url .= ':'.$this->password;				
			$url .= '@';
		}
		
		$url .= $this->domain;

		if ($path)
		{
			if ($path[0] == '#')
			{		
				$url .= $this->path;
				if ($this->query)
					$url .= '?'.$this->query;
			}
			else if ($path[0] == '?')
			{		
				$url .= $this->path;
			}
			else if ($path[0] != '/')
			{
				$i = strrpos($this->path, '/');
				if (!$i)
				{
					$url .= '/';
				}
				else
				{
					$url .= substr($this->path, 0, $i).'/';
				}
			}
			
			$url .= $path;
		}
		
		return new URL($url);
	}
	
	public function __toString()
	{
		$uri = '';
		if ($this->domain)
		{
			if ($this->scheme)
			{
				$uri .= $this->scheme.'://';
			}
		
			if ($this->username)
			{
				$uri .= $this->username;
				if ($this->password)
				{
					$uri .= ':'.$this->password;
				}
				$uri .= '@';
			}
			
			$uri .= $this->domain;
			
			if ($this->port)
				$uri .= ':'.$this->port;
		}
		
		if ($this->path)
		{
			$uri .= '/'.$this->path;
		}
		
		if ($this->query)
		{
			$uri .= '?'.$this->query;
		}
		
		if ($this->fragment)
		{
			$uri .= '#'.$this->fragment;
		}
		
		return $uri;
	}
}