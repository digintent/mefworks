<?php namespace mef\ACL;

class GraphIterator implements \Iterator
{
	public function __construct(array $graph, $base)
	{
		$this->graph = $graph;
		$this->base = $base;
	}

	public function rewind(): void
	{
		$this->visited = [];
		$this->queue = [$this->base];
		$this->i = 0;
	}

	public function valid(): bool
	{
		return $this->queue ? true : false;
	}

	public function key()
	{
		return $this->i;
	}

	public function current()
	{
		$this->role = array_shift($this->queue);

		if (isset($this->visited[$this->role]))
			throw new \Exception('Recursive graph');

		$this->visited[$this->role] = true;

		return $this->role;
	}

	public function next(): void
	{
		++$this->i;
	}

	public function queueParents()
	{
		if (isset($this->graph[$this->role]))
		{
			foreach ($this->graph[$this->role] as $parent)
				$this->queue[] = $parent;
		}
	}

}