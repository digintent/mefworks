<?php namespace mef\ACL;

class Resource implements ResourceInterface
{
	protected $id;

	public function __construct($id)
	{
		$this->id = (string) $id;
	}

	public function getResourceId()
	{
		return $this->id;
	}

	public function __toString()
	{
		return $this->id;
	}
}