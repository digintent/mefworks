<?php namespace mef\ACL;

/**
 * The ACL Manager is the core object for providing access control.
 *
 * It contains a list of grant/revoke rules and a heirarchy of roles.
 *
 */
class Manager
{
	use \mef\Getter;

	protected $grants = [];
	protected $revokes = [];

	protected $grantsByRole = [];

	/**
	 * @var array $graph - a read only array of the inheritence graph
	 */
	protected $__graph = [];

	protected function __getGraph()
	{
		return $this->__graph;
	}

	/**
	 * Grant access to the given resource for the role.
	 *
	 * The default behavior follows the "last applicable rule wins" strategy.
	 *
	 * But if $override is false, then the rule will not override any prior
	 * denies.
	 *
	 * Note that the override is not applicable to inheritence. A child role's
	 * allow rule will always override a parent's deny rule.
	 *
	 * @param string|mef\ACL\RoleInterface     $role
	 * @param string|mef\ACL\ResourceInterface $resource
	 * @param string                           $privilege
	 * @param mef\ACL\AssertionInterface       $assertion
	 * @param bool                             $override
	 */
	public function grant($role, $resource = null, $privilege = null,
		AssertionInterface $assertion = null, $override = true)
	{
		$grant = (object) [
			'role' => $this->castRole($role),
			'resource' => $this->castResource($resource),
			'privilege' => $privilege !== null ? (string) $privilege : null,
			'assertion' => $assertion,
			'exceptions' => $override ? [] : $this->revokes
		];

		$this->grants[] = $grant;
		$this->grantsByRole[$grant->role->getRoleId()][] = $grant;
	}

	/**
	 * Revoke access to the given resource for the role.
	 *
	 * All previous grants are overridden.
	 *
	 * @param string|mef\ACL\RoleInterface     $role
	 * @param string|mef\ACL\ResourceInterface $resource
	 * @param string                           $privilege
	 * @param mef\ACL\AssertionInterface       $assertion
	 */
	public function revoke($role, $resource = null, $privilege = null,
		AssertionInterface $assertion = null)
	{
		$revoke = (object) [
			'role' => $this->castRole($role),
			'resource' => $this->castResource($resource),
			'privilege' => $privilege !== null ? (string) $privilege : null,
			'assertion' => $assertion
		];

		/* Revoking some privilege effects change only if a grant was already
		 * given to the role. The easiest way to accomplish this is to add the
		 * revoking rule to each existing grant, if applicable. Then whenever
		 * a grant matches a request, the revocation list can be quickly 
		 * checked to see if any exception apply.
		 */

		foreach ($this->grants as $grant)
			$grant->exceptions[] = $revoke;

		$this->revokes[] = $revoke;
	}

	/**
	 * Adds a role to the manager.
	 *
	 * It is not a strict requirement to explicitly add roles (unless you need
	 * to define inheritence). However, it can be useful so that it is
	 * possible to query all possible roles for reporting or administration
	 * purposes.
	 *
	 * The inheritence is deferred until isAllowed() is called. i.e.,
	 * The parent's access rights are inspected during each call to
	 * isAllowed(). Therefore the parent's access rights do not have to be
	 * set up before this inherit method is called.
	 *
	 * In the case of multiple inheritence, the order is insignificant.
	 * A role's full set of permissions is the union among itself and all of
	 * its parents. A role many override its parent's permissions if it has
	 * an applicable deny rule.
	 *
	 * @param string       $child    The child role id
	 * @param string|array $parents  A string or an array of strings of 
	 *                                 the parents' role ids
	 */
	public function addRole($child, $parents = [])
	{
		$child = $this->castRoleId($child);

		if (!isset($this->graph[$child]))
		{
			$this->__graph[$child] = [];
		}

		if (!is_array($parents))
		{
			$this->__graph[$child][] = $this->castRoleId($parents);
		}
		else
		{
			foreach ($parents as $parent)
			{
				$this->__graph[$child][] = $this->castRoleId($parent);
			}
		}

		$this->__graph[$child] = array_unique($this->__graph[$child]);
	}

	/**
	 * Check to see if the role has access to the requested resource.
	 *
	 * If the resource or privilege is null, then any grant that matches
	 * the other criteria is considered applicable.
	 *
	 * e.g. If a role has access to news:view but has a deny rule for
	 * news:edit, requesting isAllowed('role', 'news') will return true
	 * because there is at least one match (news:view).
	 *
	 * @param string|mef\ACL\RoleInterface     $role
	 * @param string|mef\ACL\ResourceInterface $resource
	 * @param string                           $privilege
	 *
	 * @return boolean   true if access should be granted.
	 *                   false if access should not be granted.
	 */
	public function isAllowed($role, $resource = null, $privilege = null)
	{
		$role = $this->castRole($role);
		$resource = $this->castResource($resource);
		$privilege = $privilege ? (string) $privilege : null;

		/* Iterate over the graph starting with the requested role. At the end
		 * of each iteration, if no decision has been made, then the parents
		 * are added to the end of the list in a breadth-first fashion.
		 */

		$iterator = new GraphIterator($this->__graph, $role->getRoleId());

		foreach ($iterator as $currentRole)
		{
			$allow = null;

			$grants = isset($this->grantsByRole[$currentRole]) ?
				$this->grantsByRole[$currentRole] : [];

			foreach ($grants as $rule)
			{
				if (
					($currentRole === null || $rule->role === null || $currentRole == $rule->role->getRoleId()) &&
					($resource === null || $rule->resource === null || $resource->getResourceId() == $rule->resource->getResourceId()) &&
					($privilege === null || $rule->privilege === null || $privilege == $rule->privilege) &&
					($rule->assertion === null || $rule->assertion->assert($this, $role, $resource, $rule->privilege))
				)
				{
					/* The rule grants access to the resource.
					 * But now there may be exceptions that need to be checked.
					 */

					$allow = true;

					foreach ($rule->exceptions as $revoke)
					{
						if (
							($revoke->role === null || $currentRole == $revoke->role->getRoleId()) &&
							($revoke->resource === null || $resource->getResourceId() == $revoke->resource->getResourceId()) &&
							($revoke->privilege === null || $privilege == $revoke->privilege) && 
							($rule->assertion === null || $rule->assertion->assert($this, $role, $resource, $rule->privilege))
						)
						{
							$allow = false;
							break;
						}
					}

					/* If after, all exceptions have been examined, we still
					 * have access, then it is save to return at ths point.
					 * This works because all exceptions are attached directly
					 * to the rule, and as long as a single parent has access,
					 * so does the child.
					 */
					if ($allow)
						return true;
				}
			}

			if ($allow === null)
			{
				/* If, and only if, the end result is ambiguity do we go up 
				 * the inheritence graph. If the child has an explicit allow
				 * or deny condition, then it always overrides the parents.
				 */
				$iterator->queueParents();
			}
		}

		return false;
	}

	/**
	 * Casts the parameter to a RoleInterface
	 *
	 * @param $role string|mef\ACL\RoleInterface
	 *
	 * @return null|mef\ACL\RoleInterface
	 */
	private function castRole($role)
	{
		if (!$role instanceof RoleInterface)
		{
			$role = $role ? new Role((string) $role) : null;
		}

		return $role;
	}

	/**
	 * Casts the parameter to a ResourceInterface
	 *
	 * @param $role string|mef\ACL\ResourceInterface
	 *
	 * @return null|mef\ACL\ResourceInterface
	 */
	private function castResource($resource)
	{
		if (!$resource instanceof ResourceInterface)
		{
			$resource = $resource ? new Resource((string) $resource) : null;
		}

		return $resource;
	}

	/**
	 * Cast the parameter to a roleId string
	 *
	 * @param mixed $role
	 *
	 * @return string
	 */
	private function castRoleId($role)
	{
		if (is_string($role))
		{
			return $role;
		}
		else if ($role instanceof RoleInterface)
		{
			return $role->getRoleId();
		}
		else
		{
			return (string) $role;
		}
	}
}