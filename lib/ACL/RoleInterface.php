<?php namespace mef\ACL;

interface RoleInterface
{
	public function getRoleId();
}