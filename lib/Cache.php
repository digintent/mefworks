<?php namespace mef;

interface Cache
{
	public function set($key, $value);
	public function get($key);
	public function delete($key);
}