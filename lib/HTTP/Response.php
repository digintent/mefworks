<?php namespace mef\HTTP;

use \mef\Net\URL;

class Response extends \mef\MVC\Response
{
	protected $headers;
	protected $statusCode = 200;
	protected $statusMessage = 'OK';

	private $standardStatusMessages = [
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		306 => '',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
	];

	public function __construct()
	{
		$this->headers = new \ArrayObject();
	}

	protected function __getHeaders()
	{
		return $this->headers;
	}

	protected function __setHeaders(array $headers)
	{
		$this->headers = new \mef\ArrayObject($headers);
	}

	/**
	 * Set status code and message.
	 *
	 * Any series of new line characters in the message are replaced with a
	 * single space. Any leading or trailing space is deleted.
	 *
	 * @param int    $code    The status code (e.g., 200, 404)
	 * @param string $message The message
	 *
	 * @return  mef\HTTP\Response
	 */
	public function setStatus($code, $message = null)
	{
		if ($message === null)
		{
			$message = $this->getStatusMessageForCode($code);
		}

		$this->statusCode = (int) $code;
		$this->statusMessage = (string) preg_replace('/[\r\n]+/', ' ', trim($message));

		return $this;
	}

	/**
	 * Get the status code and message.
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return rtrim($this->statusCode . ' ' . $this->statusMessage);
	}

	/**
	 * Get the status code.
	 *
	 * @return int
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * Get the status message
	 *
	 * @return string
	 */
	public function getStatusMessage()
	{
		return $this->statusMessage;
	}

	private function getStatusMessageForCode($code, $default = '')
	{
		return isset($this->standardStatusMessages[$code]) ? $this->standardStatusMessages[$code] : $default;
	}

	/**
	 * Add redirect headers.
	 *
	 * @param  string  $url           URL to redirect to
	 * @param  integer $statusCode    default value of 302
	 * @param  string  $statusMessage status message (defaults exist for 301/302)
	 *
	 * @return  mef\HTTP\Response
	 */
	public function redirect($url, $statusCode = 302, $statusMessage = null)
	{
		if ($statusMessage === null)
		{
			$statusMessage = $this->getStatusMessageForCode($statusCode);
		}

		$this->setStatus($statusCode, $statusMessage);
		$this->headers['Location'] = (string) $url;

		return $this;
	}

	public function __toString()
	{
		$resp = "HTTP/1.1 " . $this->statusCode;
		if ($this->statusMessage !== '')
		{
			$resp .= ' ' . $this->statusMessage;
		}

		$resp .= "\r\n";
		foreach ($this->headers as $key => $val)
		{
			$resp .= "$key: $val\r\n";
		}

		$resp .= "\r\n";
		$resp .= $this->body;

		return $resp;
	}
}