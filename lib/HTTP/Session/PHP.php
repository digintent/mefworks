<?php namespace mef\HTTP;

class PHPSession extends Session
{
	protected $started = false;
	
	public function __construct($name = null)
	{
		if ($name)
			session_name($name);
	}
	
	public function start()
	{
		if ($this->started) return false;
		
		session_start();
		$this->started = true;
		
		return true;
	}
	
	public function destroy()
	{
		if (!$this->started) return false;
		
		session_destroy();
		$this->started = false;
		
		return true;
	}
	
	public function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}
	
	public function get($key)
	{
		return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
	}
	
	public function delete($key)
	{
		unset($_SESSION[$key]);
	}
	
	public function exists($key)
	{
		return array_key_exists($key, $_SESSION);
	}
	
	public function getKeys()
	{
		return array_keys($_SESSION);
	}
	
	public function asArray()
	{
		return $_SESSION;
	}
}