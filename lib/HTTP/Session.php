<?php namespace mef\HTTP;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Traversable;

abstract class Session implements ArrayAccess, IteratorAggregate
{
	abstract public function start();
	abstract public function destroy();
	abstract public function set($key, $value);
	abstract public function get($key);
	abstract public function delete($key);
	abstract public function exists($key);
	abstract public function getKeys();
	abstract public function asArray();

	public function offsetExists($key): bool
	{
		return $this->exists($key);
	}

	public function offsetGet($key)
	{
		return $this->get($key);
	}

	public function offsetSet($key, $value): void
	{
		$this->set($key, $value);
	}

	public function offsetUnset($key): void
	{
		$this->delete($key);
	}

	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->asArray());
	}
}