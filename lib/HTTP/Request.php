<?php namespace mef\HTTP;

use \Exception;
use \mef\Net\URL;

class Request extends \mef\MVC\Request
{
	protected $method;
	protected $data = [];
	protected $url;
	protected $headers = [];
	protected $params;

	private static $current = null;

	public function __construct($url, $method = 'GET', $data = [])
	{
		$this->__setURL($url);
		$this->__setMethod($method);
		$this->__setData($data);
	}

	final static public function getCurrent()
	{
		if (!self::$current)
		{
			$method = $_SERVER['REQUEST_METHOD'];
			$post = [];
			if ($method == 'POST' || $method == 'PUT' || $method == 'PATCH')
			{
				if (isset($_SERVER['CONTENT_TYPE']))
				{
					// lighttpd (others?)
					$content_type = $_SERVER['CONTENT_TYPE'];
				}
				else if (isset($_SERVER['HTTP_CONTENT_TYPE']))
				{
					// built-in webserver sets this
					$content_type = $_SERVER['HTTP_CONTENT_TYPE'];
				}
				else
				{
					$content_type = '';
				}

				if (strpos($content_type, 'multipart/form-data') !== false)
				{
					// With default settings, PHP does not populate php://input for
					// multipart/form-data.
					//
					// @todo: This behavior can be changed by setting
					// enable_post_data_reading to false in php.ini. This code
					// should actually detect that setting and always parse
					// php://input if it is set to false.
					$post = $_POST;
				}
				else
				{
					$input = file_get_contents('php://input');

					if ($input)
					{
						if (strpos($content_type, 'application/x-www-form-urlencoded') !== false)
						{
							foreach (explode('&', $input) as $var)
							{
								if (strpos($var, '=') === false)
								{
									$key = $var;
									$val = null;
								}
								else
								{
									list($key, $val) = explode('=', $var);
									$val = urldecode($val);
								}

								$key = trim(urldecode($key));

								$i = strpos($key, '[');
								if (!$i)
								{
									$post[$key] = $val;
								}
								else
								{
									try
									{
										$append = false;
										$name = substr($key, 0, $i);
										$p = &$post[$name];
										while (true)
										{
											$j = strpos($key, ']', $i + 1);
											if (!$j) throw new Exception();
											$part = substr($key, $i + 1, $j - $i - 1);
											$i = $j + 1;
											if ($part != '')
											{
												$p = &$p[$part];
												if ($i == strlen($key)) break;
												if ($key[$i] != '[') throw new Exception();
											}
											else
											{
												$append = true;
												if ($i != strlen($key)) throw new Exception();
												break;
											}
										}

										if ($append)
											$p[] = $val;
										else
											$p = $val;

										unset($p);
									}
									catch (Exception $e)
									{
									}
								}
							}
						}
						else if (strpos($content_type, 'application/json') !== false)
						{
							$post = json_decode($input, true);
							if ($post === null)
							{
								$post = [];
							}
							else if (!is_array($post))
							{
								// TODO: shouldn't assume that all POST data is an array
								$post = [$post];
							}
						}
					}
				}
			}

			if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != 'off')
			{
				$url = 'https://';
			}
			else
			{
				$url = 'http://';
			}

			$url .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

			self::$current = new self($url, $method, $post);

			foreach ($_SERVER as $key => $val)
			{
				if (substr($key, 0, 5) == 'HTTP_')
				{
					self::$current->headers[str_replace('_', '-', substr($key, 5))] = $val;
				}
			}

			if (isset(self::$current->headers['CONTENT_TYPE']) === false && isset($_SERVER['CONTENT_TYPE']) === true)
			{
				self::$current->headers['CONTENT_TYPE'] = $_SERVER['CONTENT_TYPE'];
			}
		}

		return self::$current;
	}

	protected function __setMethod($method)
	{
		if (!in_array($method, ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS']))
		{
			throw new Exception("Invalid method");
		}

		$this->method = $method;
	}

	protected function __getMethod()
	{
		return $this->method;
	}

	protected function __setData(array $data)
	{
		$this->data = $data;
	}

	protected function __getData()
	{
		return $this->data;
	}

	protected function __getParams()
	{
		return $this->params;
	}

	protected function __setURI($uri)
	{
		$this->setURL($uri);
	}

	protected function __setURL($url)
	{
		$this->url = URL::cast($url);

		parse_str($this->url->query ?? '', $this->params);

		parent::__setURI($url);
	}

	protected function __getURL()
	{
		return $this->url;
	}

	public function getHeaders()
	{
		return $this->headers;
	}
}