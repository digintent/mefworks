<?php namespace mef\UUID;

class UUID
{
	use \mef\Getter;

	private $__parts;
	private $__base64;
	private $__hex;

	public function __construct($input)
	{
		if (is_array($input))
		{
			$this->__parts = $input;
		}
		else if (strlen($input) == 22)
		{
			$raw = base64_decode(strtr($input.'==', '_-', '/+'));
			$this->__parts = array_values(unpack('n8', $raw));
		}
		else
		{
			throw new \InvalidArgumentException('parts is invalid');
		}
	}

	private function hexstr($dec)
	{
		return str_pad(dechex($dec), 4, '0', STR_PAD_LEFT);
	}

	private function __getBase64()
	{
		if ($this->__base64 == null)
		{
			$this->__base64 = '';
			foreach ($this->__parts as $part)
			{
				$this->__base64 .= chr(($part & 0xff00) >> 8).chr($part & 0xff);
			}
			$this->__base64 = strtr(substr(base64_encode($this->__base64), 0, -2), array('/' => '_', '+' => '-'));
		}

		return $this->__base64;
	}

	private function __getHex()
	{
		if ($this->__hex == null)
		{
			$this->__hex = sprintf('%s%s-%s-%s-%s-%s%s%s',
				$this->hexstr($this->__parts[0]), $this->hexstr($this->__parts[1]), 
				$this->hexstr($this->__parts[2]), 
				$this->hexstr($this->__parts[3]), 
				$this->hexstr($this->__parts[4]), 
				$this->hexstr($this->__parts[5]), $this->hexstr($this->__parts[6]), $this->hexstr($this->__parts[7])
			);
		}

		return $this->__hex;
	}

	public function __toString()
	{
		return $this->hex;
	}
}