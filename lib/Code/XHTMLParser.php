<?php namespace mef\Code;

use DomDocument;
use Exception;

class XHTMLParser
{
	private $source, $sourceLen;
	private $i, $last_i;
	private $symbol, $nextSymbol;
	private $state;
	private $dom, $cnode;
	
	private $special_vars = array(
		'Server' => '$_SERVER',
		'Get' => '$_GET',
		'Post' => '$_POST',
		'Request' => '$_REQUEST',
		'Vars' => '$this->vars',
	);
	
	const STATE_HTML = 1, STATE_TAG = 2;
	
	public function __construct($source = NULL)
	{
		if ($source)
		{
		$this->dom = new DomDocument;
		#$this->cnode = $this->dom->createElementNs('mef', 'mef:template');
#			$this->cnode = $this->dom->createElement('template');
#			$this->cnode->setAttribute('xmlns:mef', 'http://www.allegro.cc/mef');
		#$this->dom->appendChild($this->cnode);

			$this->source = $source; //file_get_contents($fn);
			$this->sourceLen = strlen($source);
			$this->symbol = $this->nextSymbol = NULL;
			$this->i = 0;
			$this->last_i = 0;
			$this->state = self::STATE_HTML;
		}
	}
	
	public function loadTemplate($fn)
	{
		$this->dom = new DomDocument;
		$this->cnode = $this->dom->createElementNs('mef', 'mef:template');
#			$this->cnode = $this->dom->createElement('template');
#			$this->cnode->setAttribute('xmlns:bs', 'http://www.allegro.cc/bs');
		$this->dom->appendChild($this->cnode);

		$this->source = file_get_contents($fn);
		$this->sourceLen = strlen($this->source);
		$this->symbol = $this->nextSymbol = NULL;
		$this->i = 0;
		$this->last_i = 0;
		$this->state = self::STATE_HTML;
	}
	
	private function getSymbol()
	{
		$this->symbol = $this->nextSymbol;
		$this->last_i = $this->i;
		
		switch ($this->state)
		{
			case self::STATE_HTML:
				$next_tag_pos = strpos($this->source, '<', $this->i);
				$next_expr_pos = strpos($this->source, '{', $this->i);
				
				if ($next_tag_pos === FALSE && $next_expr_pos === FALSE)
				{
					# rest is just plain HTML
					$next_pos = $this->sourceLen;
					
					if ($next_pos < $this->sourceLen)
					{
						$this->nextSymbol = new Symbol(Symbol::HTML, substr($this->source, $this->i, $next_pos - $this->i));
						$this->i = $next_pos;
					}
					else
						$this->nextSymbol = NULL;
				}
				else if ($next_tag_pos < $next_expr_pos || $next_expr_pos === FALSE)
				{
					# an upcoming tag.
					$next_pos = $next_tag_pos;
					
					if ($next_pos > $this->i)
					{
						$this->nextSymbol = new Symbol(Symbol::HTML, substr($this->source, $this->i, $next_pos - $this->i));
						$this->i = $next_pos;
					}
					else if ($next_pos < $this->sourceLen)
					{
						$this->state = self::STATE_TAG;
						$this->i = $next_pos + 1;
						$this->nextSymbol = new Symbol(Symbol::LT);
					}
				}
				else if ($next_expr_pos < $next_tag_pos || $next_tag_pos === FALSE)
				{
					# an upcoming expression
					$next_pos = $next_expr_pos;
					
					if ($next_pos > $this->i)
					{
						$this->nextSymbol = new Symbol(Symbol::HTML, substr($this->source, $this->i, $next_pos - $this->i));
						$this->i = $next_pos;
					}
					else if ($next_pos < $this->sourceLen)
					{
						# parse the expression, but just to see how long it is.
						$parser = new Parser_Expression(substr($this->source, $next_pos));
						$parser->parse();
						$expr_len = $parser->getIndex();
						
						$this->nextSymbol = new Symbol(Symbol::HTML, substr($this->source, $next_pos, $expr_len));
						$this->i = $next_pos + $expr_len;
						
#							var_dump($this->nextSymbol);
					}
				}
				else
					throw new Exception("Unable to find anything ...");
/*					
*/
			break;
			
			case self::STATE_TAG:
				$c = $this->source[$this->i];
				$d = $this->i < $this->sourceLen - 1 ? $this->source[$this->i + 1] : NULL;
				if ($c == '"' || $c == "'")
				{
					$i = $this->i;
					while (++$this->i < $this->sourceLen && $c != $this->source[$this->i]);
					$this->nextSymbol = new Symbol(Symbol::STRING, substr($this->source, $i + 1, $this->i - $i - 1));
					++$this->i;
				}
				else if (ctype_space($c))
				{
					$i = $this->i;
					while (++$this->i < $this->sourceLen && ctype_space($this->source[$this->i]));
					$this->nextSymbol = new Symbol(Symbol::WHITE_SPACE, substr($this->source, $i, $this->i - $i));
				}
				else if (($c >= 'A' && $c <= 'Z') || ($c >= 'a' && $c <= 'z'))
				{
					$i = $this->i;
					while (++$this->i < $this->sourceLen && (( ($c = strtolower($this->source[$this->i])) >= 'a' && $c <= 'z') || $c == '_' || ($c >= '0' && $c <= '9')) );
					$this->nextSymbol = new Symbol(Symbol::WORD, substr($this->source, $i, $this->i - $i));
				}
				else if ($c >= '0' && $c <= '9')
				{
					$i = $this->i;
					while (++$this->i < $this->sourceLen && ( ($c = $this->source[$this->i]) >= '0' && $c <= '9') );
					$this->nextSymbol = new Symbol(Symbol::NUMBER, substr($this->source, $i, $this->i - $i));
				}
				else if ($c == '>')
				{
					$this->nextSymbol = new Symbol(Symbol::GT, '>');
					$this->state = self::STATE_HTML;
					++$this->i;
				}
				else if ($d !== NULL && isset(Symbol::$map[$c.$d]))
				{
					$this->nextSymbol = new Symbol(Symbol::$map[$c.$d], $c.$d);
					$this->i += 2;
				}
				else if (isset(Symbol::$map[$c]))
				{
					$this->nextSymbol = new Symbol(Symbol::$map[$c], $c);
					++$this->i;
				}
				else
					die("WHAT??? ".htmlspecialchars($c));
			break;
		}
	}
	
	private function advancePast($text)
	{
		$i = $this->last_i;
		if (($next_pos = strpos($this->source, $text, $this->i)) === FALSE) return FALSE;
		$content = substr($this->source, $i, $next_pos-$i);
		$this->i = $next_pos + strlen($text);

		$this->state = self::STATE_HTML;
		$this->getSymbol();
		return $content;
	}
	
	private function accept()
	{
		if ($this->nextSymbol)
		{
			foreach (func_get_args() as $symbolType)
			{
				if ($this->nextSymbol->type == $symbolType)
				{
					$this->getSymbol();
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	private function expect($symbolType)
	{
		if ($this->accept($symbolType)) return TRUE;
		
		throw new Exception_ExpectSymbol($this, $symbolType, ($this->nextSymbol ? $this->nextSymbol->type : NULL));
	}
	
	private function block()
	{
		if ($this->accept(Symbol::HTML))
		{
			# block of text
			$this->cnode->appendChild($this->dom->createTextNode($this->symbol->content));
		}
		else if ($this->accept(Symbol::LT))
		{
			# entering a <tag>
			$close_tag = $this->accept(Symbol::SLASH);
			
			if ($this->accept(Symbol::BANG))
			{
				if ($this->accept(Symbol::LBRACKET))
				{
					# <![CDATA[
					$this->expect(Symbol::WORD);
					if ($this->symbol->content != 'CDATA') throw new Exception("Expecting CDATA");
					$this->expect(Symbol::LBRACKET);
					if (($content = $this->advancePast(']]>')) === FALSE)
						throw new Exception("Expecting ]]>");
					
					$this->cnode->appendChild($this->dom->createCDATASection($content));
				}
				else if ($this->accept(Symbol::MINUS))
				{
					# <!--
					$this->expect(Symbol::MINUS);
					
					if (($content = $this->advancePast('-->')) === FALSE)
						throw new Exception("Expecting -->");
					
					$tag = strtolower($this->cnode->nodeName);
					if ($tag == 'script' || $tag == 'style')
						$this->cnode->appendChild($this->dom->createTextNode($content));
				}
				else 
					throw new Exception("Unexpected token");
			}
			else
			{
				$ns = '';
				while (TRUE)
				{
					$this->expect(Symbol::WORD);
					$ns .= $this->symbol->content;
					
					if ($this->accept(Symbol::MINUS))
					{
						$ns .= '-';
						continue;
					}
					break;
				}
				
				if ($this->accept(Symbol::COLON))
				{
					$tag = '';
					while (TRUE)
					{
						$this->expect(Symbol::WORD);
						$tag .= $this->symbol->content;
						
						if ($this->accept(Symbol::MINUS))
						{
							$tag .= '-';
							continue;
						}
						break;
					}
				}
				else
				{
					$tag = $ns;
					$ns = NULL;
					
				}
				
				if (!$close_tag)
				{
					if ($ns == 'mef')
						$node = $this->dom->createElementNs('mef', $tag);
					else if ($ns) 
						$node = $this->dom->createElement($ns.':'.$tag);
					else
						$node = $this->dom->createElement($tag);

					# loop through tag attributes
					while ($this->accept(Symbol::WHITE_SPACE) && $this->accept(Symbol::WORD))
					{
						$a_ns = NULL;
						$key = '';
						
						while (TRUE)
						{
							$key .= $this->symbol->content;
							if ($this->accept(Symbol::MINUS))
							{
								$key .= '-';
								$this->expect(Symbol::WORD);
								continue;
							}
							break;
						}
						
						if ($this->accept(Symbol::COLON))
						{
							$a_ns = $key;
							$a_name = '';
							while (TRUE)
							{
								$this->expect(Symbol::WORD);
								$a_name .= $this->symbol->content;
								if ($this->accept(Symbol::MINUS))
								{
									$a_name .= '-';
									continue;
								}
								break;
							}
							$key .= ':'.$a_name;
						}
						
						$this->accept(Symbol::WHITE_SPACE);
						$this->expect(Symbol::EQUAL);
						$this->accept(Symbol::WHITE_SPACE);
						$this->expect(Symbol::STRING);
						
						if ($a_ns == 'mef')
						{
							if ($ns == $a_ns)
							{
								$node->setAttributeNs('mef', $a_name, $this->symbol->content);
							}
							else
								$node->setAttributeNs('mef', $a_ns.":".$a_name, $this->symbol->content);
						}
						else if ($a_ns)
						{
							$node->setAttributeNs($a_ns, $a_ns.":".$a_name, $this->symbol->content);
						}
						else
							$node->setAttribute($key, $this->symbol->content);
					}
					
					if (!$this->cnode)
					{
						$this->cnode = $node;
						$this->dom->appendChild($this->cnode);
					}
					else
					{
						$this->cnode->appendChild($node);
					}

					if (!$this->accept(Symbol::SLASH))
					{
						# not a self closing tag
						$this->cnode = $node;
					}
				}
				else
				{
					$name = $ns ? $ns.':'.$tag : $tag;
					if ($this->cnode->nodeName != $name)
					{
						print $this->cnode->nodeName ." vs " .$name;
						
						throw new Exception("WHOOPS");
					}
					$this->cnode = $this->cnode->parentNode;
				}
				$this->expect(Symbol::GT);
			}
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	public function parse()
	{
		$this->getSymbol();
		while ($this->block());
		
		return $this->dom;
	}
	
	/* DEBUG */
	public function getLineNumber()
	{
		return substr_count($this->source, "\n", 0, $this->last_i);
	}
	
	public function getLine($num)
	{
		$i = 0;
		while ($num-- && ($i = strpos($this->source, "\n", $i)) != FALSE) ++$i;
		if ($num != -1) return ''; # line doesn't exist
		if (($j = strpos($this->source, "\n", $i)) === FALSE) $j = strlen($this->source);
		return substr($this->source, $i, $j-$i);
	}
	
	public function getIndex()
	{
		return $this->last_i;
	}
}