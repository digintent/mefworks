<?php namespace mef\Code;

class Symbol
{
	const HTML = 1, GT = 2, WHITE_SPACE = 3, WORD = 4, COLON = 5, STRING = 6, EQUAL = 7, SLASH = 8;
	const LT = 9, LBRACE = 10, RBRACE = 11, DOT = 12, PLUS = 13, MINUS = 14, ASTERISK = 15;
	const NUMBER = 16, LBRACKET = 17, RBRACKET = 18, LPAR = 19, RPAR = 20, COMMA = 21;
	const PIPE = 22, SEMI = 23, NEQ = 24, GTE = 25, LTE = 26;
	const LAND = 27, LOR = 28, AMPERSAND = 29, QMARK = 30, BANG = 31, BOR = 32, BAND = 33;
	const PERCENT = 34;
	public $type, $content;
	
	public static $map = array(
		'<' => Symbol::LT, '>' => Symbol::GT, '=' => Symbol::EQUAL,
		'<=' => SYMBOL::LTE, '>=' => Symbol::GTE, '!=' => Symbol::NEQ, '<>' => Symbol::NEQ,
		':' => Symbol::COLON, ';' => Symbol::SEMI, '.' => Symbol::DOT, ',' => Symbol::COMMA,
		'+' => Symbol::PLUS, '-' => Symbol::MINUS, '/' => Symbol::SLASH, '*' => Symbol::ASTERISK,
		'[' => Symbol::LBRACKET, ']' => Symbol::RBRACKET, '{' => Symbol::LBRACE, '}' => Symbol::RBRACE,
		'(' => Symbol::LPAR, ')' => Symbol::RPAR, '|' => Symbol::PIPE,
		'?' => Symbol::QMARK, '&' => Symbol::AMPERSAND, '||' => Symbol::LOR, '&&' => Symbol::LAND,
		'!' => Symbol::BANG, '%' => Symbol::PERCENT
	);
	
	public function __construct($type, $content = NULL)
	{
		$this->type = $type;
		$this->content = $content;
	}
}