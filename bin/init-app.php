<?php

$options = getopt('', ['namespace:']);

if (!isset($options['namespace']))
{
	die("Must specifiy namespace with --namespace= parameter\n");
}

$namespace = $options['namespace'];

$map = ['__NAMESPACE' => $namespace];

mkdir('app', 0775);
mkdir('assets', 0775);
mkdir('bin', 0775);
mkdir('cache', 0775);
mkdir('default-config', 0775);
mkdir('etc', 0775);
mkdir('log', 0775);
mkdir('lib', 0775);
mkdir('lib/' . $namespace, 0775);
mkdir('lib/' . $namespace . '/Controller', 0775);
mkdir('lib/' . $namespace . '/Model', 0775);
mkdir('views', 0775);
mkdir('views/containers', 0775);

$self = substr(file_get_contents(__FILE__), __COMPILER_HALT_OFFSET__);
$self = array_slice(preg_split('/#### ([^\s]+)\s+/', $self, -1, PREG_SPLIT_DELIM_CAPTURE), 1);

for ($i = 0; $i < count($self); $i += 2)
{
	file_put_contents(
		strtr($self[$i], $map),
		strtr(rtrim($self[$i+1], "?> \t\n\r"), $map)
	);
}

__halt_compiler();
?>
#### bin/start-server.php
<?php
if (version_compare(PHP_VERSION, '5.5.0') < 0)
  die("Requires PHP 5.5\n");

$path = realpath(__DIR__.'/../config/application.php');
if (!$path)
	die("The config/application.php file is missing. Have you copied default-config as config yet?\n");
$config = include $path;

if (!isset($config['urls']['www']) || !isset($config['urls']['assets']))
	die("urls.www and urls.assets must both be configured\n");

function get_host_and_port($url)
{
	global $opts;

	$url = array_merge(['scheme' => '', 'path' => '', 'host' => '', 'port' => 80], parse_url($url));
	if ($url['scheme'] != 'http')
		die("Invalid scheme for $url. Only http is supported.\n");

	if ($url['path'] != '/')
		die("Invalid path for $url. Only / is supported.\n");

	return [isset($opts['bind']) ? $opts['bind'] : $url['host'], $url['port']];
}

list($assets_host, $assets_port) = get_host_and_port($config['urls']['assets']);
list($www_host, $www_port) = get_host_and_port($config['urls']['www']);

echo "Running assets on $assets_host:$assets_port\n";
$assets_pipe = popen(PHP_BINARY." -S $assets_host:$assets_port -t ".realpath(__DIR__.'/../static'), 'r');

echo "Running www on $www_host:$www_port\n";
$www_pipe = popen(PHP_BINARY." -S $www_host:$www_port ".realpath(__DIR__.'/../app/index.php'), 'r');
?>
#### default-config/application.php
<?php
return [
	'database' => [
		'dsn' => 'mysql:host=127.0.0.1;dbname=;charset=utf8mb4',
		'username' => '',
		'password' => ''
	],
	'urls' => [
		'www' => 'http://0.0.0.0:8000/',
		'assets' => 'http://0.0.0.0:8001/',
	]
];
?>
#### default-config/bootstrap.php
<?php
date_default_timezone_set('UTC');
require __DIR__.'/../vendor/autoload.php';

spl_autoload_register(function($className)
{
	$filename = __DIR__ . '/../lib/' . strtr($className, '\\_', '//') . '.php';

	if (file_exists($filename))
	{
		include $filename;
	}

});
?>
#### app/index.php
<?php
define('APPPATH', __DIR__ . '/../');
$bootstrap = include __DIR__ . '/../config/bootstrap.php';

$app = new \__NAMESPACE\Application();

$req = PHP_SAPI == 'cli' ? mef\MVC\Request\CLI::getCurrent() : mef\HTTP\Request::getCurrent();
$app->run($req);
?>
#### lib/__NAMESPACE/Application.php
<?php namespace __NAMESPACE;

class Application extends \mef\MVC\Application
{
	public $db;
	public $orm;

	public function __construct()
	{
		$router = new \mef\MVC\RouteRouter();

		// generic controller/action/id route
		$router->routes[] = new \mef\MVC\PathRoute(':controller/:action/:id');
		$router->routes[] = new \mef\MVC\DefaultRoute('Welcome', 'index');
		
		$dispatcher = new \mef\MVC\Dispatcher($router);

		$dispatcher->controllerNameFormatter = function(\mef\MVC\Request $req, \mef\MVC\MatchedRoute $matchedRoute)
		{
			return '__NAMESPACE\\Controller\\' . ucfirst($matchedRoute->controller);
		};
		
		parent::__construct($dispatcher);
		
		$this->config = include APPPATH.'config/application.php';

		$this->db = new \mef\DB\PDODriver(
			$this->config['database']['dsn'],
			$this->config['database']['username'],
			$this->config['database']['password']
		);

		$this->orm = new \mef\ORM\SQLManager($this->db);

		// Configure ORM
		$mapper = new \mef\ORM\MySQLMapper($this->db);

		foreach ($this->db->getCol('SHOW TABLES') as $tableName)
		{
			$className = '__NAMESPACE\\Model\\'.str_replace(' ', '', ucwords(str_replace('_', ' ', $tableName)));
			if (!class_exists($className))
			{
				$className = 'di\\'.str_replace(' ', '', ucwords(str_replace('_', ' ', $tableName))).'Model';

				if (!class_exists($className))
				{
					$className = 'di\\ActiveRecord';
				}
			}
			
			$this->orm->mapEntity($mapper->mapTable($tableName), $tableName, $className);
		}

		$this->acl = new \mef\ACL\Manager();

		session_start();
	}

	public function run(\mef\MVC\Request $request)
	{
		try
		{
			$response = $this->dispatcher->process($request);
			if ($response === null)
			{
				throw new \mef\MVC\FileNotFound();
			}
			
			header('HTTP/1.0 200 OK');

			foreach ($response->headers as $k => $v)
			{
				header("$k: $v");
			}
			
			echo $response->body;
		}
		catch (\mef\MVC\FileNotFound $e)
		{
			header('HTTP/1.0 404 Not Found');
			echo '404';
		}
		catch (\Exception $e)
		{
			header('HTTP/1.1 400 Bad Request');
			echo '501';
			echo $e->getMessage();
		}
	}
	
	public function getORM()
	{
		return $this->orm;
	}
}
?>
#### lib/__NAMESPACE/Controller/AbstractApplication.php
<?php namespace __NAMESPACE\Controller;

abstract class AbstractApplication extends \mef\MVC\Controller
{
	protected $orm;
	protected $container = 'containers/default.php';	
	
	public function initialize()
	{
		parent::initialize();
		
		global $app;
		$this->orm = $app->getORM();

		$this->response = new \mef\HTTP\Response();

		if (is_string($this->container))
		{
			$this->container = $this->view($this->container);
		}
	}
	
	public function before()
	{
	}
	
	public function after()
	{
		$this->response->body = (string) $this->container;
	}
	
	protected function view($filename)
	{
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		$filename = __DIR__ . '/../../../views/' . $filename;

		if ($ext == 'php')
			return new \mef\MVC\PHPView($filename, $this->view_context);
		else if ($ext == 'html')
			return new \mef\MVC\HTMLView($filename);
		else
			return new \mef\MVC\TextView($filename);
	}	
}
#### lib/__NAMESPACE/Controller/Welcome.php
<?php namespace __NAMESPACE\Controller;

class Welcome extends AbstractApplication
{
	public function indexAction()
	{
		$this->container['content'] = 'Hello, World!';
	}
}
?>
#### views/containers/default.php
<html>
<body>
	<?= $content ?>
</body>
</html>
