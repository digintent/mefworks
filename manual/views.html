<!DOCTYPE html>
<html>
<head>
<title>Mefworks Manual</title>
    
<style type="text/css">
code {
    white-space: pre;
}   
</style>
</head>
<body>
<h1>Views</h1>
<p>
    A view is a template with associated key => value pairs that can be rendered
    as a string. Four variations are provided:
</p>
<ul>
    <li>TextView: A simple substitution that replaces <tt>{foo}</tt> with the
    contents of <tt>$foo</tt>.</li>
    <li>HTMLView: An extension of TextView that sanitizes all data to make sure
    that no unsafe characters are inserted. (More on that later.)</li>
    <li>PHPView: An extension of HTMLView that allows for embedded PHP
    blocks.</li>
    <li>TemplateView: A custom XML template language with minimum presentation
    logic (e.g., looping and branching).</li>
</ul>

<h2>First Example</h2>
<code>$view = new mef\MVC\PHPView('template.php');
$view->set('user', ['name' => 'John Doe', 'age' => 42]);
echo $view;

<b>template.php</b>
&lt;p&gt;Hello, &lt;?php echo $user['name']; ?&gt;&lt;/p&gt;

&lt;?php if ($user['age'] > 40): ?&gt;
&lt;p&gt;You are older than I expected&lt;/p&gt;
&lt;?php endif; ?&gt;

<b>Output</b>
&lt;p&gt;Hello, John Doe;&lt;/p&gt;

&lt;p&gt;You are older than I expected&lt;/p&gt;
</code>

<p>
    Using views is usually that straightforward. Create an instance of the
    view you want to use, assign it some data, and then echo it to the screen.
</p>

<h2>Containers</h2>
<p>
    Within an MVC website, you probably will make use of at least one container
    view that acts as a wrapper:
</p>

<code>$container = new mef\MVC\HTMLView('container.html');
$view = new mef\MVC\PHPView('page.php');
$container->set('content', $view);
echo $container;

<b>container.html</b>
&lt;html&gt;
&lt;body&gt;
{content}
&lt;/body&gt;
&lt;/html&gt;
</code>

<p>
    There's nothing special to do. Just assign the view to the container
    (in this example, as the variable <tt>content</tt>). A container doesn't
    have to be an HTMLView. It's quite possible that it will need to be
    dynamic as well.
</p>

<p>
    The nested view will not be rendered until the container echos it out, so
    you can safely assign the view variables at any point before then. If you
    want it to be rendered at the point of assignment, you would need to
    cast it to a string:
</p>

<code>$container->content = (string) $view;</code>

<p>
    In some cases that may be useful if you want to display the same view with
    different data on the same container. The same principle applies to any
    object that has a <code>__toString</code> method.
</p>

<h2>Tainted Data</h2>

<p>
    The HTMLView and its derivatives (PHPView and TemplateView) have the
    concept of tainted data. By default, no data assigned to those views is
    trusted. For example, the string "&lt;b&gt;boldface&lt;/b&gt;" would be
    converted to "&amp;lt;b&amp;gt;boldface&amp;lt;/b&amp;gt;" such that when
    it is displayed as HTML it will appear as is. (Think
    <code>htmlspecialchars</code>.)
</p>

<p>
    Mefworks objects are built with tainting in mind, so they are usually
    converted to plain arrays and strings. This is by design! Passing
    arbitrary, complex objects to views usually leads to a violation of "best
    practices." For example, a view should not be able to modify a value of
    a model and save the changes to the database. So (by default) a typical
    model will be "exported" as a <code>stdClass</code> object at the time
    it is attached to a view. Of course there are exceptions to that rule, and
    that's why there's nothing stopping you from passing some custom HtmlWidget
    object to your view that only has functions that are safe for the view to
    call.
</p>

<p>
    This process of auto-encoding untrusted data is most useful if you store all
    of your data without any HTML encoding. If you store your data in a raw
    format (i.e., angle brackets left as &lt;), then you know you always need
    to encode it properly for the target, whatever that may be. For instance,
    if you are just logging to a console or writing a plain text email, then
    you probably don't need any encoding. But if sent to a web browser, then it
    will need HTML encoding.
</p>

<p>
    The rationale here is that in the general case, variables passed to views
    are not meant to be parsed as HTML snippets, but as raw text. If there is an
    exception to that rule, you must explicitly let the view know in one of
    several different ways:
</p>

<ul>
    <li>
    <p>Pass <code>false</code> as the third parameter to <code>set</code> to
    indicate that the variable should not be sanitized:</p>
    <code>$view->set('htmlblob', '&lt;b&gt;boldface&lt;/b&gt;', false);</code>
    </li>
    
    <li>
    <p>Make use of one of the "safe data" wrappers:</p>
    <code>$html = new mef\Validation\UntaintedScalar('&lt;b&gt;boldface&lt;/b&gt;');
$view->set('htmlblob', $html);</code>
    </li>
    
    <li>
    <p>Implement the <code>Untainted</code> interface to mark a particular class
    as always safe:</p>
    <code>class HtmlBlob implements mef\Validation\Untainted { }</code>
    <p>The <code>Untainted</code> interface does not have any methods associated
    with it; it's only used to let the view know that the data is safe.</p>
    </li>
    
    <li>
    <p>Implement the <code>Tainted</code> interface so that you can return a
    copy of the class that is safe:</p>
    <code>class HtmlBlob implements mef\Validation\Tainted
{
  public function untaint()
  {
    return new mef\Validation\UntaintedScalar(strip_unsafe_tags($this->html));
  }
}</code>
    </li>
</ul>

<p>
    In that last example, you will see that it returns an instance of
    <code>UntaintedScalar</code> (which implements <code>Untainted</code>).
    It is not strictly required that you do that. The view will happily accept
    whatever you return as an untainted piece of data that it will not further
    modify. However, if that view happens to call another view internally and
    pass that same data, it may be erraneously encoded if it is a simple string.
    By returning an instance of an untainted scalar, you can be sure that the
    data is never altered.
</p>

<h2>Context</h2>

<p>
	A PHPView is executed from within the context of the
	<code>PHPView::render()</code> function, but a closure hides the PHPView
	<code>$this</code> object from the view. By default, an empty instance of
	<code>stdClass</code> is used as <code>$this</code>. 
</p>

<p>
	Often you may find that it would be useful if your view could relay
	information back to the controller. For instance, a view may contain a title
	that belongs in an outer container or a list of JavaScript files that it
	depends on. While that information could be set in the controller, it really
	shouldn't need to be aware of those kinds of things. 
</p>

<code><b>controller.php</b>
  $view = new mef\MVC\PHPView('view.php');

  $container = new mef\MVC\PHPView('container.php');
  $container->set('contents', $view);
  
  echo $container; 

<b>view.php</b>
  &lt;?php $this->title = 'Hello, World!'; ?&gt; 
  Hello, World!
  
<b>container.php</b>
  &lt;html&gt;
  &lt;head&gt;
    &lt;title&gt;&lt;?php echo $contents->context->title; ?&gt;&lt;/title&gt; 
  &lt;/head&gt;
  &lt;body&gt;
  &lt;?php echo $contents; ?&gt;
  &lt;/body&gt;
  &lt;/html&gt;
</code>

<p>
  You can override the default context by assigning anything to the view's context
  property. Whatever you assign will become the <code>$this</code> variable: 
</p>

<code>  $view->context = new MyContext();</code>

</body>
</html>